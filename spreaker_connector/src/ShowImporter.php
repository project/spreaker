<?php

namespace Drupal\spreaker_connector;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class ShowImporter.
 *
 * @package Drupal\spreaker_connector
 */
class ShowImporter {

  use StringTranslationTrait;

  /**
   * Maps Spreaker's Show properties to Drupal's fields.
   *
   * @var string[]
   */
  private static $propertyToFieldMapping = [
    'show_id' => 'field_spreaker_show_id',
    'author' => 'field_spreaker_show_author',
    'author_id' => 'field_spreaker_show_author_id',
    'category_id' => 'field_spreaker_show_category_id',
    'description' => 'field_spreaker_description',
    'email' => 'field_spreaker_email',
    'facebook_url' => 'field_spreaker_facebook_url',
    'itunes_url' => 'field_spreaker_itunes_url',
    'image_original_url' => 'field_spreaker_show_image',
    'image_url' => 'field_spreaker_show_image_200',
    'cover_image_url' => 'field_spreaker_cover_image',
    'cover_offset' => 'field_spreaker_cover_offset',
    'last_episode_at' => 'field_spreaker_show_updated',
    'site_url' => 'field_spreaker_show_url',
    'skype_name' => 'field_spreaker_skype_username',
    'sms_number' => 'field_spreaker_text_number',
    'tel_number' => 'field_spreaker_telephone_number',
    'title' => 'title',
    'twitter_name' => 'field_spreaker_twitter_username',
    'website_url' => 'field_spreaker_contact_website',
    'language' => 'field_spreaker_show_language',
    'permalink' => 'field_spreaker_show_permalink',
    'episodes_sorting' => 'field_spreaker_episode_order',
  ];

  // Shows API URL.
  const API_SHOW_URL = 'https://api.spreaker.com/v2/shows/';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a UpdateFetcher.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   A Guzzle client object.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker Helper tools.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ClientInterface $httpClient, LoggerChannelFactory $logger, HelperTools $helperTools, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $httpClient;
    $this->logger = $logger;
    $this->helperTools = $helperTools;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * Check connection before getting the results.
   *
   * @param int $showId
   *   Spreaker show id.
   * @param bool $checkEpisodes
   *   If TRUE, will get show's episodes data.
   *
   * @return bool
   *   Returns TRUE if connection can be made. OR FALSE on fail.
   */
  public function checkConnection($showId, $checkEpisodes = FALSE) {
    try {
      // Create API url.
      $apiUrl = self::API_SHOW_URL . $showId;
      if ($checkEpisodes == TRUE) {
        $apiUrl = $apiUrl . '/episodes';
      }
      if ($this->httpClient->get($apiUrl)->getStatusCode() == '200') {
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->get('spreaker_connector')->notice($e->getMessage());
    }

    return FALSE;
  }

  /**
   * Get Show Data.
   *
   * @param int $showId
   *   Spreaker show id.
   * @param bool $getEpisodes
   *   Pull episodes of the show.
   * @param bool $alternativeUrl
   *   Alternative url for Show's episode. Currently used for NEXT page.
   *
   * @return bool|array
   *   Returns data array from spreaker or FALSE.
   */
  public function getShowData($showId, $getEpisodes = FALSE, $alternativeUrl = FALSE) {
    try {
      // Create API url.
      $apiUrl = self::API_SHOW_URL . $showId;
      if ($getEpisodes == TRUE) {
        $apiUrl = $apiUrl . '/episodes';
      }
      // If an alternative URl provided, use it.
      if ($alternativeUrl) {
        $apiUrl = $alternativeUrl;
      }
      $response = $this->httpClient->get($apiUrl);
      // Get response body.
      $responseBody = (string) $response->getBody();
      if ($responseBody) {
        $decoded = JSON::decode($responseBody);
        return isset($decoded['response']) ? $decoded['response'] : FALSE;
      }
    }
    catch (RequestException $e) {
      $this->logger->get('spreaker_connector')->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Main function to import show by providing array data from spreaker.
   *
   * @param array $data
   *   Data from spreaker.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   Return the created/updated show.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function showImport(array $data) {
    // Create a new show if it doesn't exist already.
    $show = $this->helperTools->getShowById($data['show_id']);
    if ($show == FALSE) {
      // Create show.
      $show = $this->nodeStorage->create(['type' => 'show']);
    }
    // If the fields is overridden, don't reimport the node.
    elseif ($this->helperTools->nodeIsOverridden($show)) {
      return FALSE;
    }
    $showMapping = $this->getShowFieldMappings();
    // Create node based on the mapping and data that comes in from spreaker.
    foreach ($showMapping as $spreakerProperty => $fieldName) {
      if (array_key_exists($spreakerProperty, $data)) {
        if (is_array($data[$spreakerProperty])) {
          // Author is an array. Get the title of those.
          if ($spreakerProperty == 'author') {
            if (isset($data[$spreakerProperty]['fullname'])) {
              $show->set($fieldName, $data[$spreakerProperty]['fullname']);
            }
          }
        }
        else {
          // For now we have just integer/boolean and text type fields.
          $show->set($fieldName, $data[$spreakerProperty]);
        }
      }
    }
    // If it's new, make it unpublished.
    if ($show->isNew()) {
      $show->setPublished(FALSE);
    }
    // Save the show.
    $show->save();

    return $show;
  }

  /**
   * Returns Field Mappings as an array spreaker_property|Drupal's field.
   *
   * @return array
   *   Returns mapping array.
   */
  public function getShowFieldMappings() {
    // Get the mapping.
    $mapping = self::$propertyToFieldMapping;

    // Provide the ability for modules to alter the mapping.
    \Drupal::service('module_handler')->alter('spreaker_show_mapping', $mapping);

    // Filter the mapping in case some of the fields are not on the entity.
    $mapping = $this->filterShowMapping($mapping);

    return $mapping;
  }

  /**
   * Filter fields that do not exists on show entity.
   *
   * @param array $mapping
   *   Mapping of Spreaker field to Drupal's field.
   *
   * @return array
   *   Return Filtered Mapping.
   */
  protected function filterShowMapping(array $mapping) {
    return array_filter($mapping, function ($value) {
      return $this->helperTools->entityHasField('node', 'show', $value);
    });
  }

  /**
   * Get episodes array related to the show.
   *
   * @param int $showId
   *   Spreaker show id.
   *
   * @return array
   *   Episode ids array
   */
  protected function getSprekerShowEpisodes($showId) {
    // Create episodes array.
    $episodes = [];
    if ($this->checkConnection($showId, TRUE)) {
      // Get showData about episodes.
      $data = $this->getShowData($showId, TRUE);
      if ($data) {
        // Update Episodes List.
        $episodes = $this->updateEpisodesIdList($data, $episodes);
        // Set next URL.
        $nextUrl = $data['next_url'];
        // While we have a next URL, it means we have more episodes.
        while ($nextUrl) {
          // Get next page.
          $dataNext = $this->getShowData($showId, TRUE, $nextUrl);
          // Update Episodes list.
          $episodes = $this->updateEpisodesIdList($dataNext, $episodes);
          // Update next url.
          $nextUrl = $dataNext['next_url'];
        }
      }
    }
    return $episodes;
  }

  /**
   * Update episode array list while using spreaker api.
   *
   * @param array $data
   *   Episodes ids coming from API.
   * @param array $episodes
   *   Updated episodes list.
   *
   * @return array
   *   Array of all episodes of the API. We merge each page here.
   */
  private function updateEpisodesIdList(array $data, array $episodes) {
    if (isset($data['items'])) {
      $episodeIds = array_column($data['items'], 'episode_id');
      $episodes = array_merge($episodes, $episodeIds);
    }
    return $episodes;
  }

  /**
   * Generate a batch for show's episodes import/update.
   *
   * @param array $episodes
   *   Episodes id array.
   * @param \Drupal\node\NodeInterface $show
   *   Local Show entity.
   *
   * @return array
   *   Returns the generated batch for show episodes import.
   */
  public function generateShowEpisodesBatch(array $episodes, NodeInterface $show) {
    // Sent an empty batch.
    $batch = [];
    // If we have episodes. Start with setting up the batch process.
    $numberOfEpisodes = count($episodes);
    if ($numberOfEpisodes > 0) {
      // Set operations.
      $operations = [];
      foreach ($episodes as $key => $episodeId) {
        $operations[] = [
          'spreaker_show_episodes_batch',
          [
            $episodeId,
            $this->t('(Episode @operation)', ['@operation' => $key]),
            $show,
          ],
        ];
      }
      $batch = [
        'title' => $this->t('Importing Episodes from Spreaker'),
        'operations' => $operations,
        'finished' => 'spreaker_show_episodes_batch_finished',
      ];
    }
    return $batch;
  }

  /**
   * Get show episodes.
   *
   * @param int $showId
   *   Show id from Spreaker.
   * @param string $filter
   *   New (new) or all. Will get just the episodes that are not created yet.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null|array
   *   Get show episodes based on the filter: new episodes or all episodes.
   */
  public function getShowEpisodes($showId, $filter = 'new') {
    // Get all episodes from spreaker.
    $episodes = $this->getSprekerShowEpisodes($showId);
    // Set local Show episodes id array.
    $localShowEpisodesId = [];
    // Fast fail if no episodes.
    if (empty($episodes)) {
      return NULL;
    }
    // Based on the filter, we can get either the new added episodes, or all.
    switch ($filter) {
      case 'new':
        // If the filter is new, we have to load all the episodes that have,
        // The show with this $showId referenced and compare the that list,
        // With $episodes list.
        // Get show.
        $show = $this->helperTools->getShowById($showId);
        if ($show instanceof NodeInterface) {
          // Get local episodes.
          if ($this->helperTools->entityFieldHasValue($show, 'field_spreaker_episodes')) {
            // Get referenced entities.
            $localShowEpisodes = $show->get('field_spreaker_episodes')
              ->referencedEntities();
            if (!empty($localShowEpisodes)) {
              foreach ($localShowEpisodes as $localEpisode) {
                if ($this->helperTools->entityFieldHasValue($localEpisode, 'field_spreaker_episode_id')) {
                  array_push($localShowEpisodesId, $localEpisode->field_spreaker_episode_id->value);
                }
              }
              // Filter the new episodes.
              return array_diff($episodes, $localShowEpisodesId);
            }
            else {
              return $episodes;
            }
          }
          else {
            return $episodes;
          }
        }
        break;

      case 'all':
        // Return all episodes.
        return $episodes;
    }
    return NULL;
  }

}
