<?php

namespace Drupal\spreaker_connector;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\node\NodeInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class EpisodeImporter.
 *
 * @package Drupal\spreaker_connector
 */
class EpisodeImporter {

  // Episodes API URL.
  const API_EPISODE_URL = 'https://api.spreaker.com/v2/episodes/';

  /**
   * Maps Spreaker properties to Drupal's fields.
   *
   * @var string[]
   */
  private static $propertyToFieldMapping = [
    'episode_id' => 'field_spreaker_episode_id',
    'type' => 'field_spreaker_type',
    'title' => 'title',
    'description' => 'field_spreaker_description',
    'duration' => 'field_spreaker_duration',
    'site_url' => 'field_spreaker_episode_url',
    'image_original_url' => 'field_spreaker_episode_image',
    'image_url' => 'field_spreaker_episode_image_200',
    'published_at' => 'field_spreaker_episode_date',
    'auto_published_at' => 'field_spreaker_episode_auto_date',
    'author' => 'field_spreaker_episode_author',
    'author_id' => 'field_spreaker_episode_author_id',
    'show' => 'field_spreaker_show',
    'show_id' => 'field_spreaker_show_id',
    'download_enabled' => 'field_spreaker_download_enabled',
    'downloads_count' => 'field_spreaker_downloads_count',
    'plays_ondemand_count' => 'field_spreaker_ondemand_count',
    'plays_live_count' => 'field_spreaker_plays_live_count',
    'likes_count' => 'field_spreaker_likes_count',
    'messages_count' => 'field_spreaker_messages_count',
    'is_on_air' => 'field_spreaker_is_on_air',
    'is_non_stop' => 'field_spreaker_is_non_stop',
    'encoding_status' => 'field_spreaker_encoding_status',
    'waveform_url' => 'field_spreaker_waveform_url',
    'tags' => 'field_spreaker_tags',
    'hidden' => 'field_spreaker_hidden',
    'visibility' => 'field_spreaker_visibility',
    'site_limited_url' => 'field_spreaker_site_limited_url',
    'site_limited_key' => 'field_spreaker_site_limited_key',
    'media_id' => 'field_spreaker_media_id',
    'media_url' => 'field_spreaker_media_url',
    'download_url' => 'field_spreaker_download_url',
    'playback_url' => 'field_spreaker_playback_url',
  ];

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a UpdateFetcher.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   A Guzzle client object.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker Helper tools.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ClientInterface $httpClient, LoggerChannelFactory $logger, HelperTools $helperTools, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $httpClient;
    $this->logger = $logger;
    $this->helperTools = $helperTools;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * Check connection before getting the results.
   *
   * @param int $episodeId
   *   Spreaker episode id.
   *
   * @return bool
   *   Returns TRUE if connection can be made. OR FALSE on fail.
   */
  public function checkConnection($episodeId) {
    try {
      if ($this->httpClient->get(self::API_EPISODE_URL . $episodeId)
        ->getStatusCode() == '200') {
        return TRUE;
      }
    }
    catch (RequestException $e) {
      $this->logger->get('spreaker_connector')->error($e->getMessage());
    }

    return FALSE;
  }

  /**
   * Get Episode Data.
   *
   * @param int $episodeId
   *   Spreaker episode id.
   *
   * @return bool|string
   *   Returns jsonObject or FALSE.
   */
  public function getEpisodeData($episodeId) {
    try {
      $response = $this->httpClient->get(self::API_EPISODE_URL . $episodeId);
      return (string) $response->getBody();
    }
    catch (RequestException $e) {
      $this->logger->get('spreaker_connector')->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Returns Field Mappings as an array spreaker_property|Drupal's field.
   *
   * @return array
   *   Returns mapping array.
   */
  public function getEpisodeFieldMappings() {
    // Get the mapping.
    $mapping = self::$propertyToFieldMapping;

    // Provide the ability for modules to alter the mapping.
    \Drupal::service('module_handler')->alter('spreaker_episode_mapping', $mapping);

    // Filter the mapping in case some of the fields are not on the entity.
    $mapping = $this->filterEpisodesMapping($mapping);

    return $mapping;
  }

  /**
   * Filter fields that do not exists on episode entity.
   *
   * @param array $mapping
   *   Mapping of Spreaker episode fields to drupal's episode fields.
   *
   * @return array
   *   Return Filtered Mapping.
   */
  protected function filterEpisodesMapping(array $mapping) {
    return array_filter($mapping, function ($value) {
      return $this->helperTools->entityHasField('node', 'podcast', $value);
    });
  }

  /**
   * Episode import.
   *
   * @param array $data
   *   Spreaker data array.
   * @param bool $parentShowNodeId
   *   Local show id of the node the episode should be referenced too.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   Returns newly created Episode node or FALSE.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function episodeImport(array $data, $parentShowNodeId) {
    // Create a new episode if it doesn't exist already.
    $episode = $this->helperTools->getEpisodeById($data['episode_id']);
    if ($episode == FALSE) {
      // Create episode.
      $episode = $this->nodeStorage->create(['type' => 'podcast']);
    }
    // If the fields is overridden, don't reimport the node.
    elseif ($this->helperTools->nodeIsOverridden($episode)) {
      return FALSE;
    }
    $episodeMapping = $this->getEpisodeFieldMappings();
    // Create node based on the mapping and data that comes in from spreaker.
    foreach ($episodeMapping as $spreakerProperty => $fieldName) {
      if (array_key_exists($spreakerProperty, $data)) {
        if (is_array($data[$spreakerProperty])) {
          // Author and Show and Tags are arrays. Get the title of those.
          switch ($spreakerProperty) {
            case 'author':
              if (isset($data[$spreakerProperty]['fullname'])) {
                $episode->set($fieldName, $data[$spreakerProperty]['fullname']);
              }
              break;

            case 'show':
              if (isset($data[$spreakerProperty]['title'])) {
                $episode->set($fieldName, $data[$spreakerProperty]['title']);
              }
              break;

            case 'tags':
              foreach ($data[$spreakerProperty] as $tag) {
                $episode->field_spreaker_tags->appendItem($tag);
              }
              break;
          }
        }
        else {
          // For now we have just integer/boolean and text type fields.
          $episode->set($fieldName, $data[$spreakerProperty]);
        }
      }
    }
    // Reference it to the show it belongs.
    if ($parentShowNodeId) {
      $episode->field_spreaker_parent_show->target_id = $parentShowNodeId;
    }
    // If it's new, make it unpublished.
    if ($episode->isNew()) {
      $episode->setPublished(FALSE);
    }
    // Save the episode.
    $episode->save();

    return $episode;
  }

  /**
   * Update the list of episodes for new show.
   *
   * @param int $episodeId
   *   Spreaker episode id.
   * @param object $parentShow
   *   Local show node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function fetchShowEpisode($episodeId, $parentShow) {
    // Try to check the episode.
    if ($this->checkConnection($episodeId)) {
      $data = $this->getEpisodeData($episodeId);
      // If we have data.
      if ($data) {
        $decoded = JSON::decode($data);
        if (isset($decoded['response']['episode'])) {
          // Get episode data.
          $episode_data = $decoded['response']['episode'];
          // We have to load the node each time, to get the updated one.
          $show = $this->nodeStorage->load($parentShow->id());
          if ($show instanceof NodeInterface) {
            // Import the episode.
            $episode = $this->episodeImport($episode_data, $show->id());
            if ($episode instanceof NodeInterface) {
              // First get all referenced Episodes ids.
              $referenced_episodes = array_column($show->get('field_spreaker_episodes')
                ->getValue(), 'target_id');
              if (empty($referenced_episodes)) {
                // Reference the new episode to the parent show.
                $show->get('field_spreaker_episodes')->appendItem([
                  'target_id' => $episode->id(),
                ]);
              }
              // Do not reference the new/updated episode if it already exists.
              elseif (in_array($episode->id(), $referenced_episodes) == FALSE) {
                // Filter nonexistent nodes.
                foreach ($referenced_episodes as $key => $id) {
                  if ($this->filterOutDeletedNodes($id) == FALSE) {
                    unset($referenced_episodes[$key]);
                  }
                }
                // Reference the new episode to the parent show.
                $referenced_episodes[] = [
                  'target_id' => $episode->id(),
                ];
                // Set the episodes.
                $show->get('field_spreaker_episodes')->setValue($referenced_episodes);
                // Save node.
                try {
                  $show->save();
                  // Set a log for the saved node.
                  $this->logger->get('spreaker_connector')
                    ->info('Fetched "@show_title" show and episodes form Spreaker',
                      [
                        '@show_title' => $show->getTitle(),
                      ]
                    );
                }
                catch (EntityStorageException $e) {
                  $this->logger->get('spreaker_connector')
                    ->error($e->getMessage());
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Filter out deleted nodes from reference field.
   *
   * @param int $id
   *   Node id currently referenced.
   *
   * @return bool
   *   Returns true if node exists, false otherwise.
   */
  protected function filterOutDeletedNodes($id) {
    $node = $this->nodeStorage->load($id);
    if ($node instanceof NodeInterface) {
      return TRUE;
    }
    return FALSE;
  }

}
