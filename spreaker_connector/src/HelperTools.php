<?php

namespace Drupal\spreaker_connector;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\node\NodeInterface;

/**
 * Class HelperTools. Provides helper methods.
 *
 * @package Drupal\spreaker
 */
class HelperTools {

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new BlockRepository.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field manager interface.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactory $logger, EntityFieldManagerInterface $entity_field_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->logger = $logger;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Check if entity has value.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Entity object.
   * @param string $field_name
   *   Field Name.
   *
   * @return bool
   *   Returns true of false.
   */
  public function entityFieldHasValue(FieldableEntityInterface $entity, $field_name) {
    if ($entity->hasField($field_name) && $entity->get($field_name)->isEmpty() == FALSE) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get local show id.
   *
   * @param int $showId
   *   Spreaker show ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   *   Returns Entity or false.
   */
  public function getShowById($showId) {
    $shows = $this->nodeStorage->loadByProperties([
      'type' => 'show',
      'field_spreaker_show_id' => $showId,
    ]);
    if (count($shows) > 0) {
      return reset($shows);
    }
    return FALSE;
  }

  /**
   * Checks if Episode with ID exists in Drupal's DB.
   *
   * @param int $episodeId
   *   Spreaker Episode ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   *   Returns Entity or false.
   */
  public function getEpisodeById($episodeId) {
    $episode = $this->nodeStorage->loadByProperties([
      'type' => 'podcast',
      'field_spreaker_episode_id' => $episodeId,
    ]);
    if (count($episode) > 0) {
      return reset($episode);
    }
    return FALSE;
  }

  /**
   * Check if entity has field.
   *
   * @param string $entityTypeId
   *   Entity type. Eg. Node, Paragraph, User.
   * @param string $bundle
   *   Bundle. Eg. event, show.
   * @param string $fieldName
   *   Field to check.
   *
   * @return bool
   *   Returns TRUE if exists or FALSE.
   */
  public function entityHasField($entityTypeId, $bundle, $fieldName) {
    $entityFields = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
    if (array_key_exists($fieldName, $entityFields)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if node is overridden.
   *
   * @param mixed $node
   *   Node Object or NULL.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function nodeIsOverridden($node) {
    if ($node instanceof NodeInterface) {
      if ($this->entityFieldHasValue($node, 'field_spreaker_override_api') == FALSE) {
        return FALSE;
      }
      elseif ($node->field_spreaker_override_api->value == 0) {
        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

}
