# Podcast

This module provides episode and show entities for displaying embedded podcasts,
served from the Spreaker podcast hosting platform.

Users will be able to create a show and all its episodes
by providing the show_id from Spreaker. Content will be kept synchronized with
Spreaker unless overridden in the CMS.

 - Provide a wrapper for interacting with the Spreaker API

 - Provide an admin form for managing shows

 - Continuously update show/episode details on cron

 - Prevent changes made in the CMS from being lost on subsequent imports

 - Automatically import all episodes, and their associated metadata, images,
   etc. (as a draft) when creating or updating a show

 - Automatically create entity references from the show to its episodes and
   vice-versa

 - Provide a Drupal Library for including the Spreaker player script

Override prevention
 - Once an entity has been edited in the CMS, it may be desirable to block
   further updates. To handle this an extra field is added to the entities:
   “Override API update”.
If this is off, the content will stay in sync.
