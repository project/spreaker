<?php

namespace Drupal\spreaker\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\spreaker_connector\HelperTools;
use Drupal\spreaker_connector\EpisodeImporter;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportEpisodeForm.
 *
 * @package Drupal\spreaker\From
 */
class ImportEpisodeForm extends FormBase {

  /**
   * The spreaker episode importer.
   *
   * @var \Drupal\spreaker_connector\EpisodeImporter
   */
  protected $episodeImporter;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new RestrictionPluginConfigForm object.
   *
   * @param \Drupal\spreaker_connector\EpisodeImporter $episodeImporter
   *   Episode importer service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker helper tools service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EpisodeImporter $episodeImporter, HelperTools $helperTools, EntityTypeManagerInterface $entity_type_manager) {
    $this->episodeImporter = $episodeImporter;
    $this->helperTools = $helperTools;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spreaker_connector.episode_importer'),
      $container->get('spreaker_connector.helper'),
      $container->get('entity_type.manager')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spreaker_episode_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['episode'] = [
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Import episode'),
    ];

    $form['episode']['episode_id'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Episode ID'),
      '#description' => $this
        ->t('Episode ID to import. Pull in all other episode details, and add an entity reference to the relevant show'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start import'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get Episode ID.
    $episodeId = $form_state->getValue('episode_id');
    // Try to get episode entity.
    $episode = $this->helperTools->getEpisodeById($episodeId);
    // Stop if entity was overridden.
    if ($this->helperTools->nodeIsOverridden($episode)) {
      $form_state->setError($form, $this
        ->t('This @entity_label was overridden by the user, therefore cannot be imported.', ['@entity_label' => $episode->type->entity->label()], ['context' => 'Import Show/Episode Form']));
    }
    else {
      // Check connection.
      if ($this->episodeImporter->checkConnection($episodeId)) {
        $data = $this->episodeImporter->getEpisodeData($episodeId);
        // If we have data.
        if ($data) {
          $decoded = JSON::decode($data);
          // Get show id in order to decide, either to import the episode or not.
          if (isset($decoded['response']['episode']['show_id'])) {
            $showId = $decoded['response']['episode']['show_id'];
            // Check if there is a local show with this id.
            if ($show = $this->helperTools->getShowById($showId)) {
              // Send parent show id, so e can reference to it later.
              $form['parent_show_id'] = $show->id();
              // Send data to form submit.
              $form['episode_data'] = $decoded['response']['episode'];
            }
            else {
              $form_state->setError($form, $this
                ->t('No show exists for this episode. Import the show with id: @show_id first', ['@show_id' => $showId], ['context' => 'Import Show/Episode Form']));
            }
          }
        }
      }
      else {
        $form_state->setError($form, $this
          ->t('Episode not found', [], ['context' => 'Import Show/Episode Form']));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get episode data.
    $episodeData = $form['episode_data'];
    // Get parent show.
    $showId = $form['parent_show_id'];
    if ($showId) {
      // Import episode.
      $episode = $this->episodeImporter->episodeImport($episodeData, $showId);
      // If we have the episode, set a message that that it's created/updated.
      if ($episode instanceof NodeInterface) {
        $message = new FormattableMarkup(
          "Episode: @episode_label was imported. See it <a href='@episode_url' target='_blank'>here</a>",
          [
            '@episode_label' => $episode->getTitle(),
            '@episode_url' => $episode->toUrl()->toString(),
          ]
        );
        // Create Episode URl.
        $this->messenger()
          ->addMessage($this->t('@message', ['@message' => $message]));
      }
    }
  }

}
