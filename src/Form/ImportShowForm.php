<?php

namespace Drupal\spreaker\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\spreaker_connector\HelperTools;
use Drupal\spreaker_connector\EpisodeImporter;
use Drupal\spreaker_connector\ShowImporter;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportShowForm.
 *
 * @package Drupal\spreaker\Form
 */
class ImportShowForm extends FormBase {

  /**
   * The spreaker show importer.
   *
   * @var \Drupal\spreaker_connector\ShowImporter
   */
  protected $showImporter;

  /**
   * The spreaker episode importer.
   *
   * @var \Drupal\spreaker_connector\EpisodeImporter
   */
  protected $episodeImporter;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new RestrictionPluginConfigForm object.
   *
   * @param \Drupal\spreaker_connector\ShowImporter $showImporter
   *   Show importer service.
   * @param \Drupal\spreaker_connector\EpisodeImporter $episodeImporter
   *   Episode importer service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker helper tools service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ShowImporter $showImporter, EpisodeImporter $episodeImporter, HelperTools $helperTools, EntityTypeManagerInterface $entity_type_manager) {
    $this->showImporter = $showImporter;
    $this->episodeImporter = $episodeImporter;
    $this->helperTools = $helperTools;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('spreaker_connector.show_importer'),
      $container->get('spreaker_connector.episode_importer'),
      $container->get('spreaker_connector.helper'),
      $container->get('entity_type.manager')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spreaker_show_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['show'] = [
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Import show'),
    ];

    $form['show']['show_id'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Show ID'),
      '#description' => $this
        ->t('Show ID to import. Pulls all the details of the show and creates all episodes associated with the show'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get Show ID.
    $showId = $form_state->getValue('show_id');
    // Try to get show entity.
    $show = $this->helperTools->getShowById($showId);
    // Stop if entity was overridden.
    if ($this->helperTools->nodeIsOverridden($show)) {
      $form_state->setError($form, $this
        ->t('This @entity_label was overridden by the user, therefore cannot be imported.', ['@entity_label' => $show->type->entity->label()], ['context' => 'Import Show/Episode Form']));
    }
    else {
      // Check connection.
      if ($this->showImporter->checkConnection($showId)) {
        // Get SHOW data.
        $data = $this->showImporter->getShowData($showId);
        // If we have data.
        if ($data) {
          // Send data to submit.
          $form['show_data'] = $data['show'];
          // Send show id to submit.
          $form['show_id'] = $showId;
        }
        else {
          $form_state->setError($form, $this
            ->t('No data for this show_id: @show_id found.', ['@show_id' => $showId], ['context' => 'Import Show/Episode Form']));
        }
      }
      else {
        $form_state->setError($form, $this
          ->t('Show not found', [], ['context' => 'Import Show/Episode Form']));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get Show Data.
    $showData = $form['show_data'];
    // Show id.
    $showId = $form['show_id'];
    // Import/Update show.
    $show = $this->showImporter->showImport($showData);
    // If show is a node, we should create a batch that would pull all the,
    // Show's episodes. Create/Updates those and reference those to the show.
    if ($show instanceof NodeInterface) {
      // Get batch episodes.
      $episodes = $this->showImporter->getShowEpisodes($showId, 'all');
      if (is_array($episodes) && !empty($episodes)) {
        // Generate batch.
        $batch = $this->showImporter->generateShowEpisodesBatch($episodes, $show);
        if (!empty($batch)) {
          batch_set($batch);
        }
      }
      else {
        $this->messenger()->addStatus($this->t('No Episodes found for this show ID: @show_id', ['@show_id' => $showId]));
      }
    }
  }

}
