<?php

namespace Drupal\spreaker\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\spreaker_connector\HelperTools;
use Drupal\spreaker_connector\EpisodeImporter;
use Drupal\spreaker_connector\ShowImporter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Spreaker Import action.
 *
 * @Action(
 *   id = "spreaker_import_action",
 *   label = @Translation("Spreaker Import"),
 *   type = "node",
 *   category = @Translation("Custom")
 * )
 */
class SpreakerImportAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The spreaker show importer.
   *
   * @var \Drupal\spreaker_connector\ShowImporter
   */
  protected $showImporter;

  /**
   * The spreaker episode importer.
   *
   * @var \Drupal\spreaker_connector\EpisodeImporter
   */
  protected $episodeImporter;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new RestrictionPluginConfigForm object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\spreaker_connector\ShowImporter $showImporter
   *   Show importer service.
   * @param \Drupal\spreaker_connector\EpisodeImporter $episodeImporter
   *   Episode importer service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker helper tools service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ShowImporter $showImporter, EpisodeImporter $episodeImporter, HelperTools $helperTools, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->showImporter = $showImporter;
    $this->episodeImporter = $episodeImporter;
    $this->helperTools = $helperTools;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('spreaker_connector.show_importer'),
      $container->get('spreaker_connector.episode_importer'),
      $container->get('spreaker_connector.helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($node, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $node */
    $access = $node->access('update', $account, TRUE)
      ->andIf($node->title->access('edit', $account, TRUE));
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    foreach ($entities as $entity) {
      // Continue only for show entity type.
      if ($entity->bundle() == 'show') {
        if ($this->helperTools->nodeIsOverridden($entity) == FALSE) {
          if ($this->helperTools->entityFieldHasValue($entity, 'field_spreaker_show_id')) {
            $episodes = $this->showImporter->getShowEpisodes($entity->field_spreaker_show_id->value, 'new');
            if (is_array($episodes) && !empty($episodes)) {
              // Generate batch.
              $batch = $this->showImporter->generateShowEpisodesBatch($episodes, $entity);
              if (!empty($batch)) {
                batch_set($batch);
              }
            }
          }
        }
        else {
          $message = t('@entity_label: @entity_title was overridden by the user, therefore cannot be imported.', [
            '@entity_label' => $entity->type->entity->label(),
            '@entity_title' => $entity->getTitle(),
          ],
            [
              'context' => 'Import Show/Episode Form',
            ]
          );
          $this->messenger()
            ->addWarning($this->t('@message', ['@message' => $message]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute($node = NULL) {
    /** @var \Drupal\node\NodeInterface $node */
    // Execute action.
    $this->executeMultiple([$node]);
  }

}
