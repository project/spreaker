<?php

namespace Drupal\spreaker\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\spreaker_connector\HelperTools;
use Drupal\spreaker_connector\EpisodeImporter;
use Drupal\spreaker_connector\ShowImporter;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes tasks for Statistics importer.
 *
 * @QueueWorker(
 *   id = "spreaker_importer_queue_worker",
 *   title = @Translation("Spreaker Queue Worker"),
 *   cron = {"time" = 10}
 * )
 */
class SpreakerShowImporterQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The spreaker episode importer.
   *
   * @var \Drupal\spreaker_connector\EpisodeImporter
   */
  protected $episodeImporter;

  /**
   * The spreaker show importer.
   *
   * @var \Drupal\spreaker_connector\ShowImporter
   */
  protected $showImporter;

  /**
   * Spreaker helper tools.
   *
   * @var \Drupal\spreaker_connector\HelperTools
   */
  protected $helperTools;

  /**
   * Constructs a UpdateFetcher.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\spreaker_connector\EpisodeImporter $episodeImporter
   *   Episode importer service.
   * @param \Drupal\spreaker_connector\ShowImporter $showImporter
   *   Show importer service.
   * @param \Drupal\spreaker_connector\HelperTools $helperTools
   *   Spreaker helper tools service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EpisodeImporter $episodeImporter, ShowImporter $showImporter, HelperTools $helperTools) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->episodeImporter = $episodeImporter;
    $this->showImporter = $showImporter;
    $this->helperTools = $helperTools;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('spreaker_connector.episode_importer'),
      $container->get('spreaker_connector.show_importer'),
      $container->get('spreaker_connector.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // Import/Update show.
    $show = $this->showImporter->showImport($item);
    // If show is a node, we should create a batch that would pull all the,
    // Show's episodes. Create/Updates those and reference to the show.
    if ($show instanceof NodeInterface) {
      // Get batch episodes.
      $episodes = $this->showImporter->getShowEpisodes($show->field_spreaker_show_id->value, 'new');
      if (is_array($episodes) && !empty($episodes)) {
        foreach ($episodes as $episodeId) {
          $this->episodeImporter->fetchShowEpisode($episodeId, $show);
        }
      }
    }
  }

}
